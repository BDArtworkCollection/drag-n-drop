(function() {
    
    let dndHandler = {

        draggedElement: null,

        //Drag&Drop Methods
        applyDragEvents: function(element){
            element.draggable = true;

            let dndHandler = this;
            element.addEventListener('dragstart', function(e){
                dndHandler.draggedElement = e.target;
                e.dataTranfer.setData('text/plain', '');
            });
        },

        // Gestion événements zones de drop
        applyDropEvents: function(dropper){
            dropper.addEventListener('dragover', function(e){
                e.preventDefault();

                this.className = 'dropper drop_hover';
            });
            dropper.addEventListener('dragleave', function(){
                this.className = 'dropper';
            });

            dropper.addEventListener('drop', function(e){
               let target = e.target,               
               draggedElement = dndHandler.draggedElement,
               clonedElement = draggedElement.cloneNode(true);

               while (target.className.indexOf('dropper') == -1) {
                   target = target.parentNode;
               }

               target.className = 'dropper';
               clonedElement = target.appendChild(clonedElement);
               dndHandler.applyDragEvents(clonedElement);
               draggedElement.parentNode.removeChild(draggedElement);
            });
        }

    };

        //Exec
        let elements = document.querySelectorAll('.draggable');

        for (let i = 0; i < elements.length; i++) {
            dndHandler.applyDragEvents(elements[i]);
        }

        let droppers = document.querySelectorAll('.dropper');

        for (let i = 0; i < droppers.length; i++) {
            dndHandler.applyDropEvents(droppers[i]);
        }

}) ();
